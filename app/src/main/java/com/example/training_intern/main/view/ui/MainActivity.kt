package com.example.training_intern.main.view.ui

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.training_intern.api.model.WallpaperResponse
import com.example.training_intern.api.model.WallpaperXX
import com.example.training_intern.databinding.ActivityMainBinding
import com.example.training_intern.main.view.adapter.WallpaperAdapter
import com.example.training_intern.main.viewmodel.MainViewModel
import com.example.training_intern.realm.WallpaperLocal
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmConfiguration

class MainActivity : AppCompatActivity() {
//    private val sharePreferences by lazy {
//        getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)
//    }
//    private val editor by lazy {
//        sharePreferences.edit()
//    }
    lateinit var binding: ActivityMainBinding
    lateinit var wallpapersAdapter: WallpaperAdapter
    val listData = ArrayList<WallpaperXX>()
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
//        viewModel.makeApiCallByRx(this)
        viewModel.makeApiCallCoroutines()
        initViewModel()
        setupAdapter()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initViewModel() {
        viewModel.getData(this).observe(this) { data ->
            if (data != null) {
                Log.d("trainingAPK", "makeApiCallByRxJava: ${data.size}")
                listData.addAll(data)
                wallpapersAdapter.notifyDataSetChanged()
//                editor.clear()
//                editor.apply()
//                val gson = Gson()
//                val jsonText = gson.toJson(data.data.wallpapers)
//                editor.putString("key", jsonText)
//                editor.apply()
            }
//            else {
//                val gson = Gson()
//                val jsonText: String? = sharePreferences.getString("key", "defaultName")
//                val text = gson.fromJson(
//                    jsonText,
//                    Array<WallpaperXX>::class.java
//                )
//                listData.addAll(text)
//                wallpapersAdapter.notifyDataSetChanged()
//            }
        }
    }
}