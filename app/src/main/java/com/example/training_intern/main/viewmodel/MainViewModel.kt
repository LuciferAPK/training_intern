package com.example.training_intern.main.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.training_intern.api.model.WallpaperResponse
import com.example.training_intern.api.model.WallpaperXX
import com.example.training_intern.realm.WallpaperLocal
import com.example.training_intern.repository.Repository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    var mLiveData: MutableLiveData<List<WallpaperXX>> = MutableLiveData()
    val list = ArrayList<WallpaperXX>()

    fun getData(context: Context): MutableLiveData<List<WallpaperXX>> {
        WallpaperLocal.realmConfig(context)
        return mLiveData
    }

    //Retrofit
//    fun makeApiCall(context: Context) {
//        val apiService = Repository.create().getWallPaper("vi", "medium", true, "VN", 1, 1)
//        apiService.enqueue(object : Callback<WallpaperResponse> {
//            override fun onResponse(
//                call: Call<WallpaperResponse>,
//                response: Response<WallpaperResponse>
//            ) {
//                Log.d("training_aduVip", "onResponse: ${response.body()?.data?.wallpapers}")
//                mLiveData.postValue(response.body()?.data?.wallpapers)
//                WallpaperLocal.insertData(
//                    response.body()!!.data.wallpapers
//                )
//            }
//
//            override fun onFailure(call: Call<WallpaperResponse>, t: Throwable) {
//                Log.d("trainingAPK", "onFailure: ${t.message}")
////                mLiveData.postValue(null)
//                val postValue = WallpaperLocal.getDataLocal()
//                mLiveData.postValue(postValue)
//            }
//
//        })
//    }


    //Rx
    fun makeApiCallByRx(context: Context) {
        val apiService = Repository.create().getWallPaperRx("vi", "medium", true, "VN", 1, 1)
        apiService.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(getWallpaperListRxAndroid())
    }

    private fun getWallpaperListRxAndroid(): Observer<WallpaperResponse> {
        return object : Observer<WallpaperResponse> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onNext(t: WallpaperResponse) {
                mLiveData.postValue(t.data.wallpapers)
                WallpaperLocal.insertData(t.data.wallpapers)
            }

            override fun onError(e: Throwable) {
                val postValue = WallpaperLocal.getDataLocal()
                mLiveData.postValue(postValue)
            }

            override fun onComplete() {

            }
        }
    }

    fun makeApiCallCoroutines() {
        val mainScope = CoroutineScope(Dispatchers.Main)
        mainScope.launch {
            val apiService = Repository.createCoroutines()
                .getWallPaperCoroutinesAsync("vi", "medium", true, "VN", 1, 1)
            mLiveData.postValue(apiService.body()?.data?.wallpapers)
            apiService.body()?.data?.wallpapers?.let { list.addAll(it) }
            Log.d("trainingAPK", "makeApiCallCoroutines: ${list.size}")
            apiService.body()?.data?.wallpapers?.let { WallpaperLocal.insertData(it) }

        }
//        if (list.isEmpty()) {
//            val postValue = WallpaperLocal.getDataLocal()
//            mLiveData.postValue(postValue)
//        }
    }
}
