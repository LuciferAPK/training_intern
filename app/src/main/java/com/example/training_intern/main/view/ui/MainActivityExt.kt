package com.example.training_intern.main.view.ui

import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.training_intern.main.view.adapter.WallpaperAdapter

fun MainActivity.setupAdapter() {
    wallpapersAdapter = WallpaperAdapter(listData, this, object : WallpaperAdapter.IOnClickItem {
        override fun onClickImage(position: Int) {
            Toast.makeText(this@setupAdapter, "Clicked image!!!", Toast.LENGTH_SHORT).show()
        }
    })

    binding.rvWallPaper.layoutManager = GridLayoutManager(
        this, 2,
        LinearLayoutManager.VERTICAL, false
    )
    binding.rvWallPaper.adapter = wallpapersAdapter
}