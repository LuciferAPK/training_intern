package com.example.training_intern.main.view.customview

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Handler
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.example.training_intern.BASE_VIDEO
import com.example.training_intern.R
import com.example.training_intern.api.model.WallpaperXX
import com.example.training_intern.databinding.LayoutCustomImageWallpaperBinding
import com.example.training_intern.databinding.LayoutCustomVideoWallpaperBinding
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter

class CustomItemWallPaperVideo(context: Context, atts: AttributeSet) :
    ConstraintLayout(context, atts), Player.Listener {

    private val binding: LayoutCustomVideoWallpaperBinding
    private lateinit var exoPlayer: ExoPlayer

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_custom_video_wallpaper, this, true)
        binding.videoWallpaperCustom.clipToOutline = true
        video()
    }

    private fun video() {
        setSizePlayerView()
        exoPlayer = ExoPlayer.Builder(context).build()
        binding.videoWallpaperCustom.player = exoPlayer
        exoPlayer.addListener(this)
        exoPlayer.repeatMode = Player.REPEAT_MODE_ALL
        exoPlayer.playWhenReady = true
    }

    fun addMP4Files(wallpaperXX: WallpaperXX) {
        val mediaItem = MediaItem.fromUri(BASE_VIDEO + wallpaperXX.url)
        exoPlayer.addMediaItem(mediaItem)
        exoPlayer.prepare()
    }

    private fun setSizePlayerView() {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
    }
}