package com.example.training_intern.main.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.training_intern.R
import com.example.training_intern.api.model.WallpaperXX
import com.example.training_intern.databinding.ImageItemBinding
import com.example.training_intern.databinding.VideoItemBinding
import com.example.training_intern.main.view.ui.recyclerview.RecyclerViewHolder

class WallpaperAdapter(
    private val listAdapter: ArrayList<WallpaperXX>,
    val context: Context,
    private val callback: IOnClickItem
) :
    RecyclerView.Adapter<RecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        return if (viewType == R.layout.layout_custom_image_wallpaper) {
            RecyclerViewHolder.ImageViewHolder(
                ImageItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            RecyclerViewHolder.VideoViewHolder(
                VideoItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        when (holder) {
            is RecyclerViewHolder.ImageViewHolder -> holder.bind(
                listAdapter[position],
                callback,
                position
            )
            is RecyclerViewHolder.VideoViewHolder -> holder.bind(
                listAdapter[position]
            )
        }
    }

    override fun getItemCount(): Int {
        return listAdapter.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (listAdapter[position].type) {
            "image" -> R.layout.layout_custom_image_wallpaper
            "video" -> R.layout.layout_custom_video_wallpaper
            else -> Log.d("trainingAPK", "getItemViewType: Error")
        }
    }

    interface IOnClickItem {
        fun onClickImage(position: Int)
    }
}