package com.example.training_intern.main.view.customview

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.example.training_intern.R
import com.example.training_intern.THUMB_URL
import com.example.training_intern.api.model.WallpaperXX
import com.example.training_intern.databinding.LayoutCustomImageWallpaperBinding
import com.squareup.picasso.Picasso

class CustomItemWallPaperImage(context: Context, atts: AttributeSet) :
    ConstraintLayout(context, atts) {

    private val binding: LayoutCustomImageWallpaperBinding

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_custom_image_wallpaper, this, true)
        binding.imgWallpaperCustom.clipToOutline = true
    }

    fun setImageWallpaper(wallpaperXX: WallpaperXX) {
        setSizeWallPaperImage()
        Picasso.get().load(THUMB_URL + wallpaperXX.url).into(binding.imgWallpaperCustom)
    }

    private fun setSizeWallPaperImage() {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        val deviceWidth = displayMetrics.widthPixels / 2
        binding.imgWallpaperCustom.layoutParams.width = deviceWidth
    }
}