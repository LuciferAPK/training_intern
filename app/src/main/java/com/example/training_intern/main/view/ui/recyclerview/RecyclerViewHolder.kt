package com.example.training_intern.main.view.ui.recyclerview

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.training_intern.api.model.WallpaperXX
import com.example.training_intern.databinding.ImageItemBinding
import com.example.training_intern.databinding.VideoItemBinding
import com.example.training_intern.main.view.adapter.WallpaperAdapter

sealed class RecyclerViewHolder(binding: ViewBinding) : RecyclerView.ViewHolder(binding.root) {

    class ImageViewHolder(private val binding: ImageItemBinding) : RecyclerViewHolder(binding) {
        fun bind(
            wallpaperXX_Image: WallpaperXX,
            callback: WallpaperAdapter.IOnClickItem,
            position: Int
        ) = binding.apply {
            customViewImage.setImageWallpaper(wallpaperXX_Image)
            root.setOnClickListener {
                callback.onClickImage(position)
            }
        }
    }

    class VideoViewHolder(private val binding: VideoItemBinding) : RecyclerViewHolder(binding) {
        fun bind(
            wallpaperXX_Video: WallpaperXX,
        ) = binding.apply {
            customViewVideo.addMP4Files(wallpaperXX_Video)
        }
    }
}