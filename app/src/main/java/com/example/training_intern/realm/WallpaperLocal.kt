package com.example.training_intern.realm

import android.content.Context
import com.example.training_intern.api.model.WallpaperXX
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class WallpaperLocal(
    @PrimaryKey
    var id: Int = 0,
    var name: String? = null,
    var url: String? = null,
    var type: String? = null
) : RealmObject() {

    companion object {

        fun realmConfig(context: Context) {
            Realm.init(context)
            val realmConfigRation = RealmConfiguration.Builder()
                .allowWritesOnUiThread(true)
                .schemaVersion(1)
                .build()
            Realm.setDefaultConfiguration(realmConfigRation)
        }

        fun insertData(wallpaper: List<WallpaperXX>) {
            deleteAll()
            val realm = Realm.getDefaultInstance()
            for (item in wallpaper) {
                realm.beginTransaction()
                val wallpaperLocal: WallpaperLocal =
                    realm.createObject(WallpaperLocal::class.java, item.id ?: (0..500).random())
                wallpaperLocal.name = item.name
                wallpaperLocal.url = item.url
                wallpaperLocal.type = item.type
                realm.commitTransaction()
            }
        }

        private fun deleteAll() {
            val realm: Realm = Realm.getDefaultInstance()
            realm.beginTransaction()
            realm.delete(WallpaperLocal::class.java)
            realm.commitTransaction()
        }

        fun getDataLocal() : List<WallpaperXX> {
            val realm = Realm.getDefaultInstance()
            realm.beginTransaction()
            val resultList = ArrayList<WallpaperXX>()
            val dataLocalList = realm.where(WallpaperLocal::class.java).findAll()
            for (item in dataLocalList) {
                val wallpaper = WallpaperXX()
                wallpaper.id = item.id
                wallpaper.name = item.name
                wallpaper.url = item.url
                wallpaper.type = item.type
                resultList.add(wallpaper)
            }
            return resultList
        }
    }
}