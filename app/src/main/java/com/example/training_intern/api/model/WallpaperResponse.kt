package com.example.training_intern.api.model


import com.google.gson.annotations.SerializedName

data class WallpaperResponse(
    @SerializedName("data")
    val `data`: DataX,
    @SerializedName("status")
    val status: StatusX
)