package com.example.training_intern.api.model


import com.google.gson.annotations.SerializedName

data class GroupHashTagX(
    @SerializedName("createdDate")
    val createdDate: Long,
    @SerializedName("description")
    val description: Any,
    @SerializedName("displayByLang")
    val displayByLang: String,
    @SerializedName("hashtag")
    val hashtag: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("isShow")
    val isShow: String,
    @SerializedName("isVideo")
    val isVideo: Boolean,
    @SerializedName("name")
    val name: String,
    @SerializedName("searchName")
    val searchName: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("wallCount")
    val wallCount: Int
)