package com.example.training_intern.api

import com.example.training_intern.api.model.WallpaperResponse
import io.reactivex.rxjava3.core.Observable
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {

    //https://dev.wallpapers-free.com/api/wallpapers?imagePageNumber=1&videoPageNumber=1
    @GET("/api/wallpapers")
    fun getWallPaper(
        @Header("language") language: String?,
        @Header("X-NetworkType") networkType: String?,
        @Header("X-SupportVideo") supportVideo: Boolean?,
        @Header("country") country: String?,
        @Query("imagePageNumber") imagePageNumber: Int?,
        @Query("videoPageNumber") videoPageNumber: Int?
    ): Call<WallpaperResponse>

    //https://dev.wallpapers-free.com/api/wallpapers?imagePageNumber=1&videoPageNumber=1
    @GET("/api/wallpapers")
    fun getWallPaperRx(
        @Header("language") language: String?,
        @Header("X-NetworkType") networkType: String?,
        @Header("X-SupportVideo") supportVideo: Boolean?,
        @Header("country") country: String?,
        @Query("imagePageNumber") imagePageNumber: Int?,
        @Query("videoPageNumber") videoPageNumber: Int?
    ): Observable<WallpaperResponse>

    //https://dev.wallpapers-free.com/api/wallpapers?imagePageNumber=1&videoPageNumber=1
    @GET("/api/wallpapers")
    suspend fun getWallPaperCoroutinesAsync(
        @Header("language") language: String?,
        @Header("X-NetworkType") networkType: String?,
        @Header("X-SupportVideo") supportVideo: Boolean?,
        @Header("country") country: String?,
        @Query("imagePageNumber") imagePageNumber: Int?,
        @Query("videoPageNumber") videoPageNumber: Int?
    ): Response<WallpaperResponse>
}