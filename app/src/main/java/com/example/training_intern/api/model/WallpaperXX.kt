package com.example.training_intern.api.model


import com.google.gson.annotations.SerializedName

data class WallpaperXX(
    @SerializedName("category")
    val category: String?= null,
    @SerializedName("groupHashTag")
    val groupHashTag: List<GroupHashTagX>?= null,
    @SerializedName("hashTag")
    val hashTag: String?= null,
    @SerializedName("hashtag")
    val hashtag: HashtagX?= null,
    @SerializedName("id")
    var id: Int?= null,
    @SerializedName("name")
    var name: String?= null,
    @SerializedName("type")
    var type: String?= null,
    @SerializedName("url")
    var url: String?= null
)