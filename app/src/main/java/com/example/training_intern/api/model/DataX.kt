package com.example.training_intern.api.model


import com.google.gson.annotations.SerializedName

data class DataX(
    @SerializedName("nextImagePageNumber")
    val nextImagePageNumber: Int,
    @SerializedName("nextVideoPageNumber")
    val nextVideoPageNumber: Int,
    @SerializedName("wallpapers")
    val wallpapers: List<WallpaperXX>
)