package com.example.training_intern.api.model


import com.google.gson.annotations.SerializedName

data class StatusX(
    @SerializedName("message")
    val message: String,
    @SerializedName("statusCode")
    val statusCode: Int
)